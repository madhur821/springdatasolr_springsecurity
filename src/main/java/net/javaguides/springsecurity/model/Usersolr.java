package net.javaguides.springsecurity.model;



import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.Indexed;
import org.springframework.data.solr.core.mapping.SolrDocument;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


@SolrDocument(collection ="users")
public class Usersolr {
	@Id
	@Field

     private int id;
	@Field
	 @Indexed(name = "userName", type = "string")
    private String userName;
	@Field
    private String password;
	
	@Field
    private String active;
	
	 @Indexed(name = "roles", type = "string")
    @Field
    private String roles;
    
    
	 
	public Usersolr(int id, String userName, String password, String active, String roles) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.active = active;
		this.roles = roles;
	}

	public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String isActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

	
    
}
