package net.javaguides.springsecurity.repository;



import org.springframework.data.solr.repository.Query;
import org.springframework.data.solr.repository.SolrCrudRepository;
import org.springframework.stereotype.Repository;



import net.javaguides.springsecurity.model.Usersolr;

import java.util.List;
import java.util.Optional;

@Repository
public interface usersolrRepository extends SolrCrudRepository<Usersolr, Integer> {
    Optional<Usersolr> findByUserName(String userName);
     
  
	 
	//  List<User> findByuserNameEndsWith(String userName); 
	 // List<User> findByuserNameStartsWith(String userName);



	Usersolr getUserById(int id); 
	
}
