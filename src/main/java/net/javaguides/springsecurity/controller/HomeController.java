package net.javaguides.springsecurity.controller;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import net.javaguides.springsecurity.model.Usersolr;
import net.javaguides.springsecurity.repository.usersolrRepository;



@Controller
public class HomeController {
	
	@Autowired
    usersolrRepository usersolrRepository;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(Model model, String error, String logout) {
		if (error != null)
			model.addAttribute("error", "Your username and password is invalid.");

		if (logout != null)
			model.addAttribute("message", "You have been logged out successfully.");

		return "login";
	}

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String welcome(Model model) {
		return "welcome";
	}
	
	@RequestMapping(value = "/query", method = RequestMethod.GET)
	public String querye(Model model) {
		return "search1";
	}
	
	/*
	 * @PreAuthorize("hasRole('role_searchuser')")
	 * 
	 * @RequestMapping(value = { "/searchbox" }, method = RequestMethod.GET) public
	 * String searchbox(Model model) { return "searchbox"; }
	 */
    
	/*
	 * @RequestMapping(value = { "/home" }, method = RequestMethod.GET) public
	 * String homepage(Model model) { return "home"; }
	 */
	
	@RequestMapping(value = { "/accessDenied" }, method = RequestMethod.GET)
	public String accessdenied(Model model) {
		return "accessDenied";
	}
	
	
	@RequestMapping("/users/{userName}")
	  void urluserbyid(HttpServletResponse response,@PathVariable("userName") String userName) throws IOException {
	    response.sendRedirect("http://localhost:8983/solr/users/select?q=id%3A"+userName);
	  }
	
	 @RequestMapping("/alluser")
	  void urlalluser(HttpServletResponse response) throws IOException {
	    response.sendRedirect("http://localhost:8983/solr/users/select?q=*%3A*&wt=json");
	    
	  }
	 
	 
	 @RequestMapping(value="/home")
	 private String getPrincipal(){
	        String userName = null;
	        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 
	        if (principal instanceof UserDetails) {
	            userName = ((UserDetails)principal).getUsername();
	        } 
	        if(userName=="madhur") {
	        	
	        	return "accessDenied";
	        } 
	        	 
			return "home";
	        
	       
	    }
	 
	 
	 @RequestMapping(value="/searchbox")
	 private String getPrinl(){
	        String userName = null;
	        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	 
	        if (principal instanceof UserDetails) {
	            userName = ((UserDetails)principal).getUsername();
	        } 
	        if(userName=="madhur") {
	        	
	        	return "searchbox";
	        } 
	        	 
			return "accessDenied";
	        
	       
	    }
	 
	 
	 
	 @RequestMapping("/save")
	    @ResponseBody
	    public String saveAllDocuments() {
	    //Store Documents
		 usersolrRepository.saveAll( Arrays.asList(
	    	 // new User(1,"madhur","123","true","role_searchuser"),
	          new Usersolr(6,"sh","177","true","role_user"),
	          new Usersolr(9,"abc","12","true","role_user"),
	          new Usersolr(8,"mhur","123","true","role_searchuser"),
	          new Usersolr(78,"mad","123","true","role_searchuser")));
	          return "5 documents saved!!!";
	    }
	 
	 
	 @RequestMapping("/delete")
		@ResponseBody
		 public String deleteusers() {
		     try { //delete all documents from solr core
		    	 usersolrRepository.deleteAll();
		      return "documents deleted succesfully!";
		     }catch (Exception e){
		       return "Failed to delete documents";
		     }
		 }
	 

}
