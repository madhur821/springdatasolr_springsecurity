package net.javaguides.springsecurity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import net.javaguides.springsecurity.model.User;
import net.javaguides.springsecurity.repository.UserRepository;

@SpringBootApplication
//(scanBasePackages={
		//"net.javaguides.springsecurity.Application", "net.javaguides.springsecurity.config","net.javaguides.springsecurity.controller","net.javaguides.springsecurity.model","net.javaguides.springsecurity.repository"})
public class Application implements CommandLineRunner {
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private UserRepository userRepository;
	
	public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

	@Override
	public void run(String... args) throws Exception {
		userRepository.save(new User("madhur", bCryptPasswordEncoder.encode("madhur"),"role_searchuser"));
		userRepository.save(new User("user", bCryptPasswordEncoder.encode("user"),"role_user"));
	}
}